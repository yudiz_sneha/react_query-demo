import React from 'react';
import './App.css';
import GetPost from './components/GetPosts';

function App(){
  return(
      <div className='App'>
          <GetPost />
      </div>
  )
}

export default App;