import React from 'react';
import {useQuery} from 'react-query';
import axios from 'axios';

async function fetchPosts(){
    const {data} = await axios.get('https://jsonplaceholder.typicode.com/posts')    
    return data
  }

  function GetPost(){
    const { data, error, isError, isLoading } = useQuery('posts', fetchPosts) 
  
    if(isLoading){
        return <div>Loading...</div>
    }
    if(isError){
        return <div>Error! {error.message}</div>
    }

    return(
        <div className='App'>
        <h1 style={{ textAlign: "center" }}>Posts</h1>
        {
            data.map((post, index) => {
                return <li key={index}>{post.title}</li>
            })
        }
        <br /><br />
        </div>
    )
  }
  
  export default GetPost;